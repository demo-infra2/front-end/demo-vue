const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  configureWebpack:{
    output:{
      libraryTarget:'system'
    }
  },
  devServer: {
    port: 8081
  },
  transpileDependencies: true,
  filenameHashing: false // Deshabilita la generación de hashes únicos en los nombres de archivo
})
